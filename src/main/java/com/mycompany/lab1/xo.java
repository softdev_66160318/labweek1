package com.mycompany.lab1;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author informatics
 */
import java.util.Scanner;

public class xo {
    private boolean isOver;
    char currentPlayer;
    private char[][] grid; // 3x3 grid

    public xo(char x) {
        grid = new char[3][3];
        currentPlayer = x; // X starts
        isOver = false;
        createGrid();
    }

    private void createGrid() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                grid[row][col] = ' ';
            }
        }
    }

    void printGrid() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            System.out.print("| ");
            for (int col = 0; col < 3; col++) {
                System.out.print(grid[row][col] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }

    public void play() {
        Scanner scanner = new Scanner(System.in);
        int row, col;

        while (!isOver) {
            do {
                System.out.print("Player " + currentPlayer + ", enter your move (row[1-3] column[1-3]): ");
                row = scanner.nextInt() - 1;
                col = scanner.nextInt() - 1;
            } while (!isValidMove(row, col));

            grid[row][col] = currentPlayer;
            printGrid();

            if (isWinningMove(row, col)) {
                System.out.println("Player " + currentPlayer + " wins!");
                isOver = true;
            } else if (isGridFull()) {
                System.out.println("It's a draw!");
                isOver = true;
            } else {
                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
            }
        }
        scanner.close();
    }

    private boolean isValidMove(int row, int col) {
        if (row < 0 || row >= 3 || col < 0 || col >= 3) {
            System.out.println("Invalid move! Row and column should be between 1 and 3.");
            return false;
        }
        if (grid[row][col] != ' ') {
            System.out.println("Invalid move! That cell is already occupied.");
            return false;
        }
        return true;
    }

    private boolean isWinningMove(int row, int col) {
        // Row check
        if (grid[row][0] == currentPlayer && grid[row][1] == currentPlayer && grid[row][2] == currentPlayer) {
            return true;
        }
        // Column check
        if (grid[0][col] == currentPlayer && grid[1][col] == currentPlayer && grid[2][col] == currentPlayer) {
            return true;
        }
        // Diagonal check
        if (row == col && grid[0][0] == currentPlayer && grid[1][1] == currentPlayer && grid[2][2] == currentPlayer) {
            return true;
        }
        if (row + col == 2 && grid[0][2] == currentPlayer && grid[1][1] == currentPlayer && grid[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private boolean isGridFull() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (grid[row][col] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean getStatus() {
        return isOver;
    }

    
}
    
    
    

